package org.neptune.spear


fun Byte.unsign() = java.lang.Byte.toUnsignedInt(this)

fun Short.unsign() = java.lang.Short.toUnsignedInt(this)

fun Int.unsign() = java.lang.Integer.toUnsignedLong(this)