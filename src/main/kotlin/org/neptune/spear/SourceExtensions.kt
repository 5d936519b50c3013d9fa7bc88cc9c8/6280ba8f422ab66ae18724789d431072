package org.neptune.spear

import com.sun.jna.Pointer

inline fun Source.write(address: Long, bytes: Int, writeBody: Pointer.() -> Unit) {
    val resource = MemoryCache[bytes]
    resource.writeBody()
    write(address, resource)
}

inline fun Source.write(address: Int, bytes: Int, writeBody: Pointer.() -> Unit)
        = write(address.toLong(), bytes, writeBody)

inline operator fun <reified T : Any> Source.get(address: Long, offset: Long = 0) = when (T::class.java) {
    java.lang.Byte::class.java -> byte(address, offset)
    java.lang.Short::class.java -> short(address, offset)
    java.lang.Character::class.java -> char(address, offset)
    java.lang.Integer::class.java -> int(address, offset)
    java.lang.Long::class.java -> long(address, offset)
    java.lang.Float::class.java -> float(address, offset)
    java.lang.Double::class.java -> double(address, offset)
    java.lang.Boolean::class.java -> boolean(address, offset)
    else -> throw IllegalArgumentException()
} as T

inline operator fun <reified T : Any> Source.get(address: Int, offset: Long = 0): T
        = get(address.toLong(), offset)